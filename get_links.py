# -*- coding: utf-8 -*-


'''
    Script que extrae los links de las publicaciones de un sitio web 
'''
from bs4 import BeautifulSoup
import codecs, urllib, os, re, time
import urllib2
import json, io, csv
from tabulate import tabulate

def get_index(url):
    '''
        funcion que obtien el numero total de paginas en un sitio web 
    '''
    try:
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        html = opener.open(url)
    except IOError:
        print "wait..."
        time.sleep(2)
    else:
        soup = BeautifulSoup(html, 'html.parser')
        try:
            pag = soup.find('div', {'pagination'})
        except AttributeError:
            print "error"
        else:
            index = int(re.findall(r'<span>Página 1 de (\d+)</span>', str(pag))[0])
            return index


if __name__ == '__main__':

    categories = 'arte fotografia cine historia letras musica diseno tecnologia estilo-de-vida viajes comida'
    file = 'test.txt'
    authors_dict = {}
    for cat in categories.split():
        print "Extrayendo de:", cat
        url = 'http://culturacolectiva.com/category/'+ cat +'/page/1/'
        authors_dict[cat] = {}
        for x in xrange(1, get_index(url)+1):
            try:
                url = 'http://culturacolectiva.com/category/'+ cat +'/page/' + str(x) + '/'
                opener = urllib2.build_opener()
                opener.addheaders = [('User-agent', 'Mozilla/5.0')]
                html = opener.open(url)
            except IOError:
                print "wait..."
                time.sleep(2)
            else:
                soup = BeautifulSoup(html, 'html.parser')
                try:
                    text = soup.find('ul', {'archive-list'})
                except AttributeError:
                    print 'error'
                else:
                    li_tags = [x for x in text.children if x!='\n']
                    for li in li_tags:
                        link = str(re.findall(r'<h2><a href="(.+?)">', str(li))[0])
                        author = str(re.findall(r'rel="author" title="Entradas de (.+?)">', str(li))[0])
                        if author != 'Cultura Colectiva':
                            if author not in authors_dict[cat]:
                                authors_dict[cat][author] = [link]
                            else:
                                authors_dict[cat].get(author).append(link)
    table = []
    file = open('table.txt', 'w')
    for cat in authors_dict:
        for autor in authors_dict.get(cat):
            table.append([cat.decode('utf-8'), autor.decode('utf-8'), len(authors_dict[cat].get(autor))])
    file.write(tabulate(table, headers=['Categoria','Autor', '# files'], tablefmt="plain").encode("utf-8") )


    with open('all_links_cc.json', 'w') as file:
        json.dump(authors_dict, file, sort_keys=True, indent=4, ensure_ascii=False)


    print "Done"






