# -*- coding: utf-8 -*-

'''
    Script que extrae el contenido de publicaciones automaticamente, este modulo hace uso de una recopilacion de links, los cuales son generados con el script que acompaña a este.
    
'''

from bs4 import BeautifulSoup as bs
from bs4.element import NavigableString
import codecs, os, re, time
import urllib2
import json, io, csv
from tabulate import tabulate
import unicodedata
import thread
from threading import Thread

errors = {}

def elimina_tildes(s):
    '''
        Funcion que elimina las tildes para poder ser procesado
    '''
   return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))


def get_text_post(url, ruta):
    '''
        Funcion extrae todo el cuerpo de una piblicacion 
    '''
    correct = True
    tries = 0
    while correct:
        try:
            opener = urllib2.build_opener()
            opener.addheaders = [('User-agent', 'Mozilla/5.0')]
            html = opener.open(url)
        except IOError:
            time.sleep(2)
            tries += 1
        else:
            correct = False
            soup = bs(html, 'html.parser')
            title = bs(html, 'html.parser')
            try:
                html_title = soup.find('div', {'id': 'title-main'})
                content_area = soup.find('div', {'id': 'content-area'})
            except AttributeError:
                # Error en la estructura del html 
                print 'error en:', url
            else:
                post_content = []
                ls_tags = []
                # variable para almacenar temporalmente el nombre de cada publicacion 
                title = html_title.find('h1')
                post_content.append(title.get_text() + '\n\n')
                # atributos a buscar en cada etiqueta div
                div_attrs = ['id', 'class']
                # variables de nombres de clases a buscar en cada etiqueta div
                div_class = ['astrata-toolbar','show-desktop', 'show-mobile', 'taboola-below-article']
                # discriminar si el contenido es codigo javascript
                for n in content_area.children:
                    if n.name != 'script':
                        ls_tags.append(n)
                # extraccion de texto en funcion a la estructura del sitio web
                for index,tag in enumerate(ls_tags):
                    if tag.name != None:
                        if 'class' in tag.attrs:
                            if tag['class'][0] in div_class:
                                ls_tags.pop(index)
                        elif 'id' in tag.attrs:
                            if tag['id'] in div_class:
                                ls_tags.pop(index)
                for tag in ls_tags:
                    if not isinstance(tag, NavigableString):
                        for node in tag.children:
                            if node.name == None:
                                post_content.append(node.strip())
                            elif node.name == 'br':
                                post_content.append('\n')
                            else:
                                post_content.append(node.get_text())
                    elif len(tag) > 0 and tag != '\n' and tag != '':
                        post_content.append(tag)
                for i,p in enumerate(post_content):
                    if p in ['\n', '\t', '\s']:
                        post_content.pop(i)
                text = ' '.join(post_content).strip()
                if len(text) < 200:
                    print 'Error', url, ruta
                f = codecs.open(ruta, 'w', 'utf-8')
                f.write(text)

if __name__ == '__main__':

    # leer archivo con los links de cada pagina web
    with open('links.json', 'r') as fp:
        data = json.load(fp, strict=False)
    # Iterar por cada autor que escribe en el sitio web 
    for author in data:
        print 'current author ------>', author
        for category in data[author]:
            print '\t category ---->',category
            # crea directorio
            autor = elimina_tildes(author)
            autor = autor.replace(' ', '_')
            corpus_path = 'cultura\\{0}\\{1}\\'.format(category, autor)
            if not os.path.exists(corpus_path):
                os.makedirs(corpus_path)
            # para aumentar la velocidad con la que se extrae la informacíon 
            # se crean multiples hilos para resolver diferentes peticiones a la vez
            ls_thread=[]
            for num,link in enumerate(data[author].get(category)):
                path = corpus_path + str(num) + '.txt'
                th = Thread(target=get_text_post, args=(link, path))
                th.start()
                ls_thread.append(th)
            for t in ls_thread:
                t.join()
            # liberar hilos 

    # with open('log_errors.json', 'w') as file:
    #     json.dump(errors, file, sort_keys=True, indent=4)


