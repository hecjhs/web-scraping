# README #

Este es un ejemplo de un script para poder extraer información automática de un sitio web, este script solo fue empleado para propósitos académicos.

Este script esta pensado para trabajar de forma particular con la estructura de un sitio web, de tal forma que si dicha estructura cambia, el funcionamiento de este script no está garantizada. 

Fecha de implementación marzo de 2015. 


### How do I get set up? ###
Primer paso es correr el script "get_links", de tal manera que se genera una base de datos

Segundo es correr el script "get_post", el es el encargado de crear el corpus.